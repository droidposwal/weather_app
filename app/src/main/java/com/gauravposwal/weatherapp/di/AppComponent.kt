package com.gauravposwal.weatherapp.di

import com.gauravposwal.weatherapp.ui.MainActivity
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [NetworkModule::class,ViewModelModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)

}