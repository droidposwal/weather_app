package com.gauravposwal.weatherapp.base

import android.app.Application
import com.gauravposwal.weatherapp.di.DaggerAppComponent

class MyApplication : Application() {
    val appComponent = DaggerAppComponent.create()

}