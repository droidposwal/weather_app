package com.gauravposwal.weatherapp.network

import com.gauravposwal.weatherapp.model.CurrentWeatherResponse
import com.gauravposwal.weatherapp.model.ForecastWeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface WeatherService {
    companion object{
        const val  BASE_URL=" http://api.openweathermap.org/data/2.5/"
        const val CITY_ID="1277333"
        const val APPID="709dcf880fc4cbc705bbc62b2825b164"
        const val UNITS="metric"

    }
//    http://api.openweathermap.org/data/2.5/weather?id=1277333&APPID=709dcf880fc4cbc705bbc62b2825b164

    @GET("weather?")
    fun getCurrentWeather(@Query("id")id:String=CITY_ID, @Query("units")units:String= UNITS,
                          @Query("APPID") appID:String= APPID):
            Call<CurrentWeatherResponse>

//    http://api.openweathermap.org/data/2.5/forecast/daily?id=1277333&APPID=709dcf880fc4cbc705bbc62b2825b164
    @GET("forecast/daily?")
    fun getForecastWeather(@Query("id")id:String=CITY_ID, @Query("units")units:String= UNITS,
                           @Query("APPID") appID:String= APPID):
    Call<ForecastWeatherResponse>
}