package com.gauravposwal.weatherapp.ui

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.gauravposwal.weatherapp.R
import com.gauravposwal.weatherapp.Utils
import com.gauravposwal.weatherapp.base.MyApplication
import com.gauravposwal.weatherapp.model.CurrentWeatherResponse
import com.gauravposwal.weatherapp.model.ForecastWeatherResponse
import com.gauravposwal.weatherapp.model.WeatherForecastData
import com.gauravposwal.weatherapp.viewmodel.WeatherViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {


    lateinit var weatherViewModel: WeatherViewModel

    /**
     * Injecting via dagger
     * Profit is that we don't need to create every different viewModelFactory class with different params
     */
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val weatherForecastDataList = ArrayList<WeatherForecastData>()
    private lateinit var recyclerAdapter: RecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {

        (applicationContext as MyApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        weatherViewModel =
            ViewModelProviders.of(this, viewModelFactory)[WeatherViewModel::class.java]

        handleRotation(savedInstanceState)

        setupRecyclerView()
        btRetry.setOnClickListener {
            handleRetry()
        }

    }

    private fun handleRotation(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            // handling rotation
            weatherViewModel.forecastList?.apply {
                weatherForecastDataList.addAll(this)
                animateBottomSheet()
            }
            weatherViewModel.currentWeather?.apply {
                updateUi(weatherViewModel.currentWeather!!)
            }

        }
    }

    private fun handleRetry() {
        showErrorScreen(false)  // hide error screen
        showLoader(true)        // show loader
        getCurrentWeatherData() // get the current weather data form server
        getForecastWeatherData()  // get the feature list weather data from the server
    }


    private fun setupRecyclerView() {

        val linearLayoutManager = LinearLayoutManager(this)
        rvWeather.layoutManager = linearLayoutManager
        recyclerAdapter = RecyclerAdapter(weatherForecastDataList)
        rvWeather.adapter = recyclerAdapter
        // for line seperation between items
        val dividerItemDecoration =
            DividerItemDecoration(rvWeather.context, linearLayoutManager.orientation)
        rvWeather.addItemDecoration(dividerItemDecoration)
    }

    /**
     * this start animation when we show the loader
     * clear the animation when we don't show the loader
     */
    private fun showLoader(flag: Boolean) {
        if (flag) {
            image.visibility = View.VISIBLE
            val aniRotate: Animation = AnimationUtils.loadAnimation(this, R.anim.rotate_loader)
            image.startAnimation(aniRotate)

        } else {
            image.visibility = View.GONE
            image.clearAnimation()

        }
    }


    private fun updateUi(currentWeather: CurrentWeatherResponse) {
        Log.d("MainActivity", currentWeather.main!!.toString())
        tvTemp.text = "${currentWeather.main.temp.toInt()}${getString(R.string.degree_symbol)}"
        tvMinMaxTemp.text = Utils.generateMinMaxTemperature(
            currentWeather.main.tempMax,
            currentWeather.main.tempMin,
            getString(R.string.degree_symbol)
        )
        tvCity.text = currentWeather.name

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.weather_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_fetch -> {
                showErrorScreen(false)
                showLoader(true)
                getCurrentWeatherData() // get the current weather data form server
                getForecastWeatherData()  // get the feature list weather data from the server

            }
        }
        return true
    }

    private fun getCurrentWeatherData() {
        if (Utils.isOnline(this))
            weatherViewModel.getWeatherData().observe(this, Observer<CurrentWeatherResponse> {
                showLoader(false) // hide loader
                if (it != null){
                    weatherViewModel.setCurrentWeatherData(it)
                    updateUi(it)
                }
                else showErrorScreen(true)
            })
        else showErrorScreen(true, offline = true)
    }

    private fun showErrorScreen(flag: Boolean, offline: Boolean = false) {
        llError.visibility = if (flag) View.VISIBLE else View.GONE
        tvErrorMessage.text =
            if (offline) getString(R.string.no_network_connection) else getString(R.string.something_went_wrong)

    }

    private fun getForecastWeatherData() {
        if (Utils.isOnline(this))
            weatherViewModel.getForecastWeatherData().observe(
                this,
                Observer<ForecastWeatherResponse> {
                    if (it != null) {
                        weatherViewModel.setForecastWeatherList(it.list!!)
                        populateBottomSheet(it.list)
                    } else showErrorScreen(true)
                })
        else showErrorScreen(true, offline = true)

    }

    private fun populateBottomSheet(newData: ArrayList<WeatherForecastData>) {
        //we want to show the next 4 data and first data is of current date so..
        // remove the first data
        newData.removeAt(0)
        recyclerAdapter.updateList(newData)
        animateBottomSheet()
    }

    private fun animateBottomSheet() {
        val transition: Transition = Slide(Gravity.BOTTOM)
        transition.duration = 600
        transition.addTarget(R.id.llWeatherData)

        TransitionManager.beginDelayedTransition(clRootLayout, transition)
        showBottomSheet(true)

    }

    private fun showBottomSheet(flag: Boolean) {

        llWeatherData.visibility = if (flag) View.VISIBLE else View.GONE
    }
}
