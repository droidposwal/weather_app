package com.gauravposwal.weatherapp.ui

import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.gauravposwal.weatherapp.R
import com.gauravposwal.weatherapp.Utils.generateMinMaxTemperature
import com.gauravposwal.weatherapp.model.WeatherForecastData
import kotlinx.android.synthetic.main.weather_item_row.view.*
import java.util.*
import kotlin.collections.ArrayList


class RecyclerAdapter(private val weatherList: ArrayList<WeatherForecastData>) :
    RecyclerView.Adapter<RecyclerAdapter.WeatherHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherHolder {
        val inflatedView = parent.inflate(R.layout.weather_item_row, false)
        return WeatherHolder(inflatedView)
    }

    override fun getItemCount() = 4

    override fun onBindViewHolder(holder: WeatherHolder, position: Int) {
        if (weatherList.size > 0) {

            val itemPhoto = weatherList[position]
            holder.bindPhoto(itemPhoto)
        }
    }

    fun updateList(newData: ArrayList<WeatherForecastData>) {
        weatherList.clear()
        weatherList.addAll(newData)
        notifyDataSetChanged()

    }


    class WeatherHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        private lateinit var forecastData: WeatherForecastData
        private var view: View = v

        init {

            v.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            Log.d("RecyclerView", "CLICK!")
        }

        fun bindPhoto(forecastData: WeatherForecastData) {
            this.forecastData = forecastData
            view.tvDay.text = getDayNameFromTimeStamp(forecastData.dt)
            view.tvTemp.text = generateMinMaxTemperature(
                forecastData.temp?.max!!,
                forecastData.temp.min, view.context.getString(R.string.degree_symbol)
            )
        }


        private fun getDayNameFromTimeStamp(timeStamp: Long): String {

            val calendar = Calendar.getInstance(Locale.ENGLISH)
            calendar.timeInMillis = timeStamp * 1000L
            return DateFormat.format("EEEE", calendar).toString()
        }
    }


}

private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

