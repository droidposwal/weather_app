package com.gauravposwal.weatherapp.model

import com.google.gson.annotations.SerializedName


data class CurrentWeatherResponse(
    @SerializedName("coord")
    val coord: Coord? = null,
    @SerializedName("weather")
    val weather: ArrayList<Weather>? = null,
    @SerializedName("base")
    val base: String? = null,
    @SerializedName("main")
    val main: Main? = null,
    @SerializedName("wind")
    val wind: Wind? = null,
    @SerializedName("clouds")
    val clouds: Clouds? = null,
    @SerializedName("dt")
    val dt: Int,
    @SerializedName("sys")
    val sys: Sys? = null,
    @SerializedName("timezone")
    val timezone: Int,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("cod")
    val cod: Int
)

data class Wind(@SerializedName("speed") val speed: Double, @SerializedName("deg") val deg: Int)
data class Weather(
    @SerializedName("id") val id: Int,
    @SerializedName("main") val main: String? = null,
    @SerializedName("description") val description: String? = null,
    @SerializedName("icon") val icon: String? = null
)

data class Sys(
    @SerializedName("country")
    val country: String? = null,
    @SerializedName("sunrise")
    val sunrise: Int,
    @SerializedName("sunset") val sunset: Int
)

data class Main(
    @SerializedName("temp")
    val temp: Double,
    @SerializedName("feels_like")
    val feelsLike: Double,
    @SerializedName("temp_min")
    val tempMin: Double,
    @SerializedName("temp_max")
    val tempMax: Double,
    @SerializedName("pressure")
    val pressure: Int,
    @SerializedName("humidity")
    val humidity: Int,
    @SerializedName("sea_level")
    val seaLevel: Int,
    @SerializedName("grnd_level")
    val grndLevel: Int
)

data class Coord(
    @SerializedName("lon")
    val lon: Double,
    @SerializedName("lat")
    val lat: Double
)

data class Clouds(
    @SerializedName("all")

    val all: Int
)


//------------------ Forecast Weather -------------------//

data class ForecastWeatherResponse(

    @SerializedName("city")
    val city: City? = null,

    @SerializedName("cod")
    val cod: String? = null,

    @SerializedName("message")
    val message: Double,

    @SerializedName("cnt")
    val cnt: Int,

    @SerializedName("list")
    val list: ArrayList<WeatherForecastData>? = null
)

data class Temp(
    @SerializedName("day")
    val day: Double,
    @SerializedName("min")
    val min: Double,
    @SerializedName("max")
    val max: Double,
    @SerializedName("night")
    val night: Double,
    @SerializedName("eve")
    val eve: Double,
    @SerializedName("morn")
    val morn: Double
)


data class WeatherForecastData(
    @SerializedName("dt")
    val dt: Long,

    @SerializedName("sunrise")
    val sunrise: Int,

    @SerializedName("sunset")
    val sunset: Int,

    @SerializedName("temp")
    val temp: Temp? = null,

    @SerializedName("feels_like")
    val feelsLike: FeelsLike? = null,

    @SerializedName("pressure")
    val pressure: Int,

    @SerializedName("humidity")
    val humidity: Int,

    @SerializedName("weather")
    val weather: ArrayList<Weather>? = null,

    @SerializedName("speed")
    val speed: Double,

    @SerializedName("deg")
    val deg: Int,

    @SerializedName("clouds")
    val clouds: Int
)

data class FeelsLike(
    @SerializedName("day")
    val day: Double,

    @SerializedName("night")
    val night: Double,

    @SerializedName("eve")
    val eve: Double,

    @SerializedName("morn")
    val morn: Double
)

data class City(
    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String? = null,

    @SerializedName("coord")

    val coord: Coord? = null,
    @SerializedName("country")

    val country: String? = null,
    @SerializedName("population")

    val population: Int,

    @SerializedName("timezone")
    val timezone: Int
)