package com.gauravposwal.weatherapp.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gauravposwal.weatherapp.network.WeatherService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class WeatherDataRepository @Inject constructor(private val weatherService: WeatherService) {

    // hit the apis here

    fun getCurrentWeatherData(): LiveData<CurrentWeatherResponse> {
        val currentWeatherData: MutableLiveData<CurrentWeatherResponse> = MutableLiveData()
        weatherService.getCurrentWeather().enqueue(object : Callback<CurrentWeatherResponse> {
            override fun onResponse(
                call: Call<CurrentWeatherResponse>,
                response: Response<CurrentWeatherResponse?>
            ) {
                if (response.isSuccessful) {
                    currentWeatherData.value = response.body()
                } else {
                    currentWeatherData.value = null
                }
            }

            override fun onFailure(call: Call<CurrentWeatherResponse>, t: Throwable?) {
                currentWeatherData.value = null
            }
        })
        return currentWeatherData

    }

    /**
     * get the forecast weather data of 4 days
     */
    fun getForecastWeatherData(): LiveData<ForecastWeatherResponse> {
        val forecastWeatherData: MutableLiveData<ForecastWeatherResponse> = MutableLiveData()
        weatherService.getForecastWeather().enqueue(object : Callback<ForecastWeatherResponse> {
            override fun onResponse(
                call: Call<ForecastWeatherResponse>,
                response: Response<ForecastWeatherResponse>
            ) {
                if (response.isSuccessful) {
                    forecastWeatherData.value = response.body()
                } else {
                    forecastWeatherData.value = null
                }
            }

            override fun onFailure(call: Call<ForecastWeatherResponse>, t: Throwable) {
                forecastWeatherData.value = null

            }

        })
        return forecastWeatherData


    }

}