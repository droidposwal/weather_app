package com.gauravposwal.weatherapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.gauravposwal.weatherapp.model.CurrentWeatherResponse
import com.gauravposwal.weatherapp.model.ForecastWeatherResponse
import com.gauravposwal.weatherapp.model.WeatherDataRepository
import com.gauravposwal.weatherapp.model.WeatherForecastData
import javax.inject.Inject

class WeatherViewModel @Inject constructor(private val weatherRepo: WeatherDataRepository) :
    ViewModel() {

    var currentWeather: CurrentWeatherResponse? = null
    var forecastList: ArrayList<WeatherForecastData>? = null

    fun getWeatherData(): LiveData<CurrentWeatherResponse> {

        return weatherRepo.getCurrentWeatherData()
    }

    fun getForecastWeatherData(): LiveData<ForecastWeatherResponse> {

        return weatherRepo.getForecastWeatherData()
    }

    fun setCurrentWeatherData(currentWeather: CurrentWeatherResponse) {
        this.currentWeather = currentWeather

    }

    fun setForecastWeatherList(forecastList: ArrayList<WeatherForecastData>) {
        this.forecastList = forecastList
    }

}

